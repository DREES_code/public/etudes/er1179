# ER1179
Ce dossier fournit les programmes permettant de reproduire les bases de données, illustrations et chiffres de l'Études et résultats n° 1179 de la DREES : « Un quart des parents non gardiens solvables ne déclarent pas verser de pension alimentaire à la suite d’une rupture de Pacs ou d’un divorce », janvier 2021.

Un ensemble de 13 programmes est mis à disposition :
- Le programme 0_initialisation.R définit l’environnement, les paramètres et les chemins. Ce code doit tourner au début de chaque nouvelle session R. L’utilisateur remplacera la localisation de l’environnement de travail définie dans la commande « setwd » par son propre chemin d’accès aux programmes et « path » par le chemin d’accès aux tables de l’EDP.
- Les programmes data_part1.R et data_part2.R créent respectivement les bases de données de la partie 1 de l’ER sur le taux de non-versement et de la partie 2 sur les montants déclarés et la comparaison au barème. Il suffit de les faire tourner une seule fois chacun. Les tables sont alors enregistrées dans le dossier « data » créé là où les programmes se situent. Ces deux programmes appellent les codes a_mariages_pacs.R, b_enfants.R, c_pac.R et d_variables.R. Une fois que les base de données sont enregistrées, tous les autres programmes peuvent tourner indépendamment les uns des autres.
- Les programmes tableau_1.R, tableau_2.R, tableau_3.R et graphiques.R exportent les données nécessaires à la création des principales figures de l’ER. Ces informations sont enregistrées au format Excel dans le dossier « resultats » créé là où les codes sont situés.
- Le code divers.R fournit les autres chiffres mentionnés dans l’ER.
- Le code fonctions.R regroupe les fonctions utilisées par les autres programmes.


Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/un-quart-des-parents-non-gardiens-solvables-ne-declarent-pas-verser-de-pension

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Échantillon Démographique Permanent (INSEE), version de juin 2019. Traitements : Drees. Ces données sont disponibles en accès sécurisé via le CASD : https://www.casd.eu/en/source/permanent-demographic-sample/
Lien vers la documentation de l’EDP : https://utiledp.site.ined.fr/

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.0.2, le 19/01/2021.
