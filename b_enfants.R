################################################################################
#
# Copyright (C) 2021. Logiciel �labor� par l'�tat, via la Drees.
#
# Nom de l'auteur : Rapha�l Lardeux, Drees.
#
# Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire 
# les figures publi�es dans l'�tudes et R�sultats 1179, intitul� � Un quart des 
# parents non gardiens solvables ne d�clarent pas verser de pension alimentaire 
# � la suite d'une rupture de Pacs ou d'un divorce�, janvier 2021. 
#
# Ce programme a �t� ex�cut� le 19/01/2021 avec la version 4.0.2 de R et les 
# packages : haven v2.3.1, lubridate v1.7.9, data.table v1.13.0, xlsx v0.6.4.2, 
# xtable v1.8-4, AER v1.2-9, car v3.0-10, plm v2.2-4, lmtest v0.9-38, 
# sampleSelection v1.2-6, maxLik v1.4-4.
#
# Le texte et les figures de l'�tude peuvent �tre consult�s sur le site de la 
# DREES : https://drees.solidarites-sante.gouv.fr/IMG/pdf/er1179.pdf
# 
# Ce programme utilise les donn�es de l'Echantillon D�mographique Permanent (EDP)
# sur la p�riode 2011 � 2017, dans leur version de juin 2019. La documentation de
# l'EDP est disponible sur le site de l'Ined : https://utiledp.site.ined.fr/
#
# Bien qu'il n'existe aucune obligation l�gale � ce sujet, les utilisateurs de 
# ce programme sont invit�s � signaler � la DREES leurs travaux issus de la 
# r�utilisation de ce code, ainsi que les �ventuels probl�mes ou anomalies 
# qu'ils y rencontreraient, en �crivant � DREES-CODE@sante.gouv.fr
# 
# Ce logiciel est r�gi par la licence "GNU General Public License" GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s
# au chargement, � l'utilisation, � la modification et/ou au d�veloppement et �
# la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de 
# logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc 
# � des d�veloppeurs et des professionnels avertis poss�dant des connaissances 
# informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
# tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
# d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
# g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
# connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.
#
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

################################################################################
##########                                                          ############
##########           R�cup�rer l'information sur les enfants        ############
##########                                                          ############
################################################################################



### Tous les enfants en vie n�s entre 2004 et la date de la rupture:

enf     <- enf0[(substr(enf_ind_nai_date,1,4)>= dn_min) & (substr(enf_ind_nai_date,1,4)<= an_i) & (evt_type!="ESV")]
enf     <- merge(edp_i[,c("id_diff","dn_i","dn_cj_i")], enf, by="id_diff")

# Filtre : uniquement les enfants issus des deux parents dont on �tudie la s�paration. 
# On enl�ve donc les cas o� la date de naissance d'un des parents est mal connue, ou 
# bien enfants d'un seul des deux parents qui se s�parent:

enf[,same_date := ifelse((ope_type=="PERE")&(substr(dn_cj_i,1,10)==substr(mere_ind_nai_date,1,10)),1,0)]
enf[,same_date := ifelse((ope_type=="MERE")&(substr(dn_cj_i,1,10)==substr(pere_ind_nai_date,1,10)),1,same_date)]                          
enf            <- enf[same_date==1]


### Passage au format wide:

# Cr�ation de variables importantes par la suite:
# ynais     = ann�e de naissance des enfants concern�s par une pension alimentaire
# nbenf_min = nombre d'enfants mineurs concern�s par une pension alimentaire
# benj      = ann�e de naissance du plus jeune enfant du couple
# sexe_maj  = majorit� de gar�ons (=1) ? de filles (=2) ? autant des deux (=3) ?

enf[,ynais     := as.numeric(substr(enf_ind_nai_date,1,4))] 
enf[,sexe      := ifelse(enf_ind_sexe=="M",1,2)]
enf[,c("enf_ind_nai_date","enf_ind_sexe") := NULL]
enf            <- enf[,c("id_diff","ynais","sexe")]
enf            <- enf[order(id_diff,ynais)]
enf[,nbenf_min := .N, by = id_diff]
enf[,benj      := max(ynais), by = id_diff]
enf[,sum_g     := sum(ifelse(sexe==1,1,0)), by = id_diff]
enf[,sexe_maj  := ifelse(sum_g==0.5*nbenf_min, 3, ifelse(sum_g<0.5*nbenf_min,2,1))]
enf[,child     := ave(ynais,id_diff,FUN = seq_along)]
enf[,sum_g     := NULL]

enf2          <- dcast(enf, id_diff + nbenf_min + benj + sexe_maj ~ child, value.var = c("ynais","sexe"))

edp_i         <- merge(edp_i,enf2,by="id_diff",all=T)
edp_i         <- edp_i[!is.na(edp_i$id_diff)]

