################################################################################
#
# Copyright (C) 2021. Logiciel �labor� par l'�tat, via la Drees.
#
# Nom de l'auteur : Rapha�l Lardeux, Drees.
#
# Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire 
# les figures publi�es dans l'�tudes et R�sultats 1179, intitul� � Un quart des 
# parents non gardiens solvables ne d�clarent pas verser de pension alimentaire 
# � la suite d'une rupture de Pacs ou d'un divorce�, janvier 2021. 
#
# Ce programme a �t� ex�cut� le 19/01/2021 avec la version 4.0.2 de R et les 
# packages : haven v2.3.1, lubridate v1.7.9, data.table v1.13.0, xlsx v0.6.4.2, 
# xtable v1.8-4, AER v1.2-9, car v3.0-10, plm v2.2-4, lmtest v0.9-38, 
# sampleSelection v1.2-6, maxLik v1.4-4.
#
# Le texte et les figures de l'�tude peuvent �tre consult�s sur le site de la 
# DREES : https://drees.solidarites-sante.gouv.fr/IMG/pdf/er1179.pdf
# 
# Ce programme utilise les donn�es de l'Echantillon D�mographique Permanent (EDP)
# sur la p�riode 2011 � 2017, dans leur version de juin 2019. La documentation de
# l'EDP est disponible sur le site de l'Ined : https://utiledp.site.ined.fr/
#
# Bien qu'il n'existe aucune obligation l�gale � ce sujet, les utilisateurs de 
# ce programme sont invit�s � signaler � la DREES leurs travaux issus de la 
# r�utilisation de ce code, ainsi que les �ventuels probl�mes ou anomalies 
# qu'ils y rencontreraient, en �crivant � DREES-CODE@sante.gouv.fr
# 
# Ce logiciel est r�gi par la licence "GNU General Public License" GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s
# au chargement, � l'utilisation, � la modification et/ou au d�veloppement et �
# la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de 
# logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc 
# � des d�veloppeurs et des professionnels avertis poss�dant des connaissances 
# informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
# tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
# d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
# g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
# connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.
#
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

################################################################################
#########                                                               ########    
#########               Initialisation de l'environnement               ########
#########                                                               ########  
################################################################################
rm(list=ls())

### Environnement:

setwd("[chemin vers le r�pertoire de travail]")


### Localisation des donn�es:

path  <- "[chemin vers le r�pertoire des donn�es EDP]//edp_be2017_"


### Packages:

library(haven)
library(lubridate)
library(data.table)
library(xlsx)
library(xtable)
library(AER)
library(car)
library(plm)
library(lmtest)
library(sampleSelection)

### Param�tres du bar�me 2010 de la Justice:

# RSA socle 2011 � 2016 (moyenne pour les ann�es avec revalorisation en septembre):
rsa_socle   <- c(466.99, 474.93,(8*483.24 + 4*492.90)/12,(8*499.31 + 4*509.30)/12,(8*513.88 + 4*524.16)/12,(3*524.16+5*524.68 +4*535.17)/12)

# IPC 2011 � 2016 (valeur en juillet):
ipc         <- c(96.79,98.6,99.5,99.87,100.03,100.26)

# Taux de pension par enfant selon le nombre d'enfants:
bareme      <- c(0.135,0.115,0.100,0.088,0.080,0.072)



### Cr�ation de dossiers:

dir.create("data", showWarnings = F)        # sauvegarde des bases temporaires
dir.create("results", showWarnings = F)     # fichiers excel de sorties

